//=====================================================================================
//                                                                                   //
//                 ESP32 Weathercloud Weather Station Firmware                       //
//                                                                                   //
//                       Developed by Jakub Nagy, 2020                               //
//                       Program under the MIT license                               //
//                                                                                   //
//=====================================================================================
//User setup (change these variables according to your needs)
const char* ssid     = "your-wifi-ssid";      //Type in your Wi-Fi network SSID (name)
const char* password = "your-wifi-password";    //Type in your Wi-Fi network password
const char* Weathercloud_ID  = "your-weathercloud-key";                    //Copy and paste your Weathercloud ID here
const char* Weathercloud_KEY = "your-weathercloud-key";    //Copy and paste your Weathercloud KEY here

const long  gmtOffset_sec = 3600;        //Put here your local time offset in seconds (example GMT+1 = +1 * 3600s = 3600s)
const int   daylightOffset_sec = 3600;   //Do you have daylight saving time? Yes = 3600, No = 0
//=====================================================================================
//Wi-Fi and NTP client setup
#include <WiFi.h>
WiFiClient client;

const int httpPort = 80;
const char* Weathercloud = "http://api.weathercloud.net";
const char* streamId   = "....................";
const char* privateKey = "....................";

void wifi_connect()
{
    int connectTries = 0;
       
    WiFi.mode(WIFI_OFF);
    delay(200);
    WiFi.mode(WIFI_STA);
    
    Serial.print("Connecting to "); Serial.println(ssid);
    WiFi.begin(ssid, password);
    while(WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");

        if(connectTries > 20) wifi_connect();
        connectTries++;
    } 
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println(" ");
}
//=====================================================================================
//NTP client setup
#include "time.h"
struct tm timeinfo;

const char* ntpServer = "pool.ntp.org";
RTC_DATA_ATTR int lastminute, lasthour, lastday;

void get_local_time()
{
  Serial.println("Pulling local time from NTP server.");
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.print("Time pulled from NTP server sucessfuly. Time: ");
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S\n");
}
//=====================================================================================
//Deep sleep
void sleep(int sleeptime){
    Serial.println("Weather station going to sleep...");
    esp_sleep_enable_timer_wakeup(sleeptime * 1000000);
    delay(1000);
    Serial.flush(); 
    esp_deep_sleep_start();
}
//=====================================================================================
//temperature
#include <OneWire.h>
#include <DallasTemperature.h>
#define TEMPERATURE_BUS 4

OneWire oneWire(TEMPERATURE_BUS); 
DallasTemperature sensors(&oneWire);
DeviceAddress groundThermometer = { 0x28,  0xD4,  0xB0,  0x26,  0x0,  0x0,  0x80,  0xBC };
DeviceAddress airThermometer = { 0x28,  0xF4,  0xBC,  0x26,  0x0,  0x0,  0x80,  0x2B };

float tempair, tempground;
int temp, tempin;

void read_temperature()
{
    sensors.requestTemperatures();
    delay(10);
    tempair = sensors.getTempC(airThermometer);
    tempground = sensors.getTempC(groundThermometer);
    temp = int(tempair*10);
    tempin = int(tempground*10);
}
//=====================================================================================
//humidity and pressure
#include <Wire.h>
#include <BME280I2C.h>
BME280I2C bme;

#define c1 -8.78469475556
#define c2 1.61139411
#define c3 2.33854883889
#define c4 -0.14611605
#define c5 -0.012308094
#define c6 -0.0164248277778
#define c7 0.002211732
#define c8 0.00072546
#define c9 -0.000003582

float tempbme(0), humidity(0), pressure(0), heatIndex;
int bar, hum, heat, dew;

void read_humidity_pressure()
{
    bme.read(pressure, tempbme, humidity, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
    bar = int(pressure/10);
    hum = int(humidity);

    heatIndex = c1 + (c2 * tempair) + (c3 * humidity) + (c4 * tempair * humidity) + (c5 * sq(tempair)) + (c6 * sq(humidity)) + (c7 * sq(tempair) * humidity) + (c8 * tempair * sq(humidity)) + (c9 * sq(tempair) * sq(humidity));
    heat = int(heatIndex*10);

    double gamma = log(humidity / 100) + ((17.62 * tempair) / (243.5 + tempair));
    double dewPoint = (243.5 * gamma / (17.62 - gamma));
    dew = int(dewPoint*10);
}
//=====================================================================================
//solar radiation
#include <BH1750.h>
BH1750 lightMeter;

float lux;
int solarrad;

void read_solarRadiation()
{
    lux = lightMeter.readLightLevel();
    solarrad = lux*0.79;
}
//=====================================================================================
//UV radiation
#define UV_INPUT 34

float uvVoltage, uvIndex;
int uvi;

void read_uvRadiation()
{
    uvVoltage = (analogRead(UV_INPUT) * (3.3 / 4095));
    uvIndex = (uvVoltage - 1)* 7.5;
    if(uvVoltage > 2.46) uvIndex = 11;
    if(uvVoltage < 1) uvIndex = 0;
    uvi = int(uvIndex*10);
}
//=====================================================================================
//wind speed and direction
#define WSPD_INTERRUPT 26
#define WDIR_INPUT 35

boolean state = false;
unsigned long lastMillis = 0;
float mps, kph, windChill;
int clicked, wspd, wdir, wdirRaw, wchill;

void read_windSpeed()
{
    Serial.println("Starting wind speed measurment period.");

    lastMillis = xTaskGetTickCount();
    while(xTaskGetTickCount() - lastMillis < 10000){
        if(digitalRead(WSPD_INTERRUPT) == HIGH) if(state == false){
            delay(50);
            clicked++;
            state = true;
        }
        if(digitalRead(WSPD_INTERRUPT) == LOW) state = false;
    }

    mps = clicked * 0.0333;
    kph = mps * 3.6;
    wspd = int(mps*10);
    clicked = 0;

    Serial.println("Wind speed measurement complete.\n");

    windChill = (13.12 + (0.62 * tempair) - (11.37 * (pow(kph, 0.16))) + (0.39 * tempair * (pow(kph, 0.16))));
    wchill = int(windChill*10);
    if(mps < 0.5) wchill = temp;
    if(tempair > 30) wchill = temp;
}

void read_windDirection()
{
    wdirRaw = analogRead(WDIR_INPUT);

    if(wdirRaw > 2155 && wdirRaw < 2207) wdir = 0;
    else if (wdirRaw > 502  && wdirRaw < 554) wdir = 22.5;
    else if (wdirRaw > 649  && wdirRaw < 699) wdir = 45;
    else if (wdirRaw > 49 && wdirRaw < 90) wdir = 112.5;
    else if (wdirRaw > 89 && wdirRaw < 111) wdir = 135;
    else if (wdirRaw > 0 && wdirRaw < 11) wdir = 157.5;
    else if (wdirRaw > 242 && wdirRaw < 280) wdir = 180;
    else if (wdirRaw > 159 && wdirRaw < 211) wdir = 202.5;
    else if (wdirRaw > 1239 && wdirRaw < 1301) wdir = 225;
    else if (wdirRaw > 1129 && wdirRaw < 1161) wdir = 247.5;
    else if (wdirRaw > 4069 && wdirRaw < 5001) wdir = 270;
    else if (wdirRaw > 2470 && wdirRaw < 2516) wdir = 292.5;
    else if (wdirRaw > 3101 && wdirRaw < 3321) wdir = 315;
    else if (wdirRaw > 1629 && wdirRaw < 1656) wdir = 337.5;
    else if(wdirRaw == 0) wdir = 90;
}
//=====================================================================================
//rainfall
RTC_DATA_ATTR int input, rainfall, rainrate;

void read_rainfall()
{
    Wire.requestFrom(105, 4);
    while (Wire.available()){ 
        input = Wire.read();
        if(input == 1){
            rainfall++;
            rainrate++;
            delay(100);
        }
    }
    input = 0;

    if(timeinfo.tm_hour != lasthour) rainrate = 0;
    if(timeinfo.tm_mday != lastday) rainfall = 0;
  
    lasthour = timeinfo.tm_hour;
    lastday = timeinfo.tm_mday;
}
//=====================================================================================
void print_data()
{
    Serial.print("Air temperature [°C]: "); Serial.println(temp / 10);
    Serial.print("Ground temperature [°C]: "); Serial.println(tempin / 10);
    Serial.print("Wind chill [°C]: "); Serial.println(wchill / 10);
    Serial.print("Dew point [°C]: "); Serial.println(dew / 10);
    Serial.print("Heat index [°C]: "); Serial.println(heat / 10);
    Serial.print("Humidity [%]: "); Serial.println(hum);
    Serial.print("Wind speed [m/s]: "); Serial.println(wspd / 10);
    Serial.print("Wind direction [°]: "); Serial.println(wdir);
    Serial.print("Barometric pressure [hPa]: "); Serial.println(bar / 10);
    Serial.print("Rainfall (per day) [mm/m2]: "); Serial.println(rainfall * 3);
    Serial.print("Rainrate (per hour) [mm/m2]: "); Serial.println(rainrate * 3);
    Serial.print("UV index [Index]: "); Serial.println(uvi / 10);
    Serial.print("Solar radiation [W/m2]: "); Serial.println(solarrad / 10);
    Serial.println();
}
//=====================================================================================
//Weathercloud push function
void push_to_weathercloud()
{
    Serial.println("Initializing data push to Weathercloud.");

    if (!client.connect(Weathercloud, httpPort)) {
        Serial.println("Connecting to Weatherloud failed.\n");
        return;
    }
    
    client.print("GET /set");
    client.print("/wid/"); client.print(Weathercloud_ID);
    client.print("/key/"); client.print(Weathercloud_KEY);
    
    client.print("/temp/"); client.print(temp);
    client.print("/tempin/"); client.print(tempin);
    client.print("/chill/"); client.print(wchill);
    client.print("/dew/"); client.print(dew);
    client.print("/heat/"); client.print(heat);
    client.print("/hum/"); client.print(hum);
    client.print("/wspd/"); client.print(wspd);
    client.print("/wdir/"); client.print(wdir);
    client.print("/bar/"); client.print(bar);
    client.print("/rain/"); client.print(rainfall*3);
    client.print("/rainrate/"); client.print(rainrate*3);
    client.print("/uvi/"); client.print(uvi);
    client.print("/solarrad/"); client.print(solarrad);

    client.println("/ HTTP/1.1");
    client.println("Host: api.weathercloud.net");
    client.println("Connection: close");
    client.println();
    Serial.println("Data pushed to Weathercloud sucessfuly.\n");;
}
//=====================================================================================
//setup
void setup()
{
    Serial.begin(115200);
    Serial.println("\nWeather station powered on.\n");
    wifi_connect();
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

    sensors.begin();
    sensors.getAddress(airThermometer, 1); 
    sensors.getAddress(groundThermometer, 0); 
    sensors.setResolution(airThermometer, 15);
    sensors.setResolution(groundThermometer, 15);

    Wire.begin();
    bme.begin();
    lightMeter.begin();
    pinMode(WSPD_INTERRUPT, INPUT);
}
//=====================================================================================
//loop
void loop()
{
    get_local_time();

    read_rainfall();
    read_temperature();
    read_humidity_pressure();
    read_solarRadiation();
    read_uvRadiation();
    read_windSpeed();
    read_windDirection();

    print_data();

    push_to_weathercloud();

    sleep(60); 
}
//=====================================END OF CODE=====================================
