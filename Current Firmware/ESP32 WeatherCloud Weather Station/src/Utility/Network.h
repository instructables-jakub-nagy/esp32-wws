/*!
*    @file Network.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to manage the network activity of the station. This includes connecting
*    to a Wi-fi network, getting time from a NPT server and pushing data to Weathercloud.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    2) Packet.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    3) SerialInterface.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    4) ESP32 Wifi library (a default ESP32 library in the Arduino framework)
*    5) Time library (a default ESP32 library in the Arduino framework)
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// User config
#include<UserConfig.h>

// Project libraries
#include<Utility/Utils.h>
#include<Utility/Packet.h>
#include<Utility/SerialInterface.h>

// ESP32 network libraries
#include <WiFi.h>
#include <time.h>


// Weathercloud API config
static const int httpPort = 80;
static const char* Weathercloud = "http://api.weathercloud.net";

// NTP server config
const char* ntpServer = "pool.ntp.org";


// Manages the station network activity 
class Network {
public:
    // Initialize the network interface
    static void begin()
    {
        // Reset the RF chip
        disable_rf();
        delay(100);
        enable_rf();

        // Init the NTP server
        configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    }

    // Connects to a wi-fi network
    static void connect()
    {
        SerialInterface::print("Connecting to "); Serial.println(ssid);
        
        // Start connecting to Wi-fi network
        WiFi.begin(ssid, password);

        int connect_tries = 0;

        while(WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(".");

            if(connect_tries > 20) connect();
            connect_tries++;
        }

        SerialInterface::empty_line(1);
        SerialInterface::println("WiFi connected");
        SerialInterface::print("IP address: ");
        SerialInterface::println(WiFi.localIP());
        SerialInterface::empty_line(1);
    }

    // Retrieves local time from a NPT server
    void get_local_time()
    {
        SerialInterface::println("Pulling local time from NTP server.");
        if(!getLocalTime(&timeinfo)){
            SerialInterface::println("Failed to obtain time");
            return;
        }
        SerialInterface::print("Time pulled from NTP server successfully. Time: "); Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S\n");
    }

    // Sends a packet of data to Weathercloud
    static void send(Packet packet)
    {
        SerialInterface::println("Initializing data push to Weathercloud.");

        // Try to connect to the Weathercloud API server
        if (!client.connect(Weathercloud, httpPort)) {
            SerialInterface::println("Connecting to Weatherloud failed.\n");
            return;
        }

        // Compressess the data packet before it is sent 
        packet.compress();
        
        // Initialize the GET request
        client.print("GET /set");

        // Add the Weathercloud authentication tokens
        client.print("/wid/");      client.print(Weathercloud_ID);
        client.print("/key/");      client.print(Weathercloud_KEY);
        
        // Add sensor values
        client.print("/temp/");     client.print(packet.temperature);
        client.print("/tempin/");   client.print(packet.temperature_inside);
        client.print("/chill/");    client.print(packet.wind_chill);
        client.print("/dew/");      client.print(packet.dew_point);
        client.print("/heat/");     client.print(packet.heat_index);
        client.print("/hum/");      client.print(packet.humidity);
        client.print("/wspd/");     client.print(packet.wind_speed);
        client.print("/wdir/");     client.print(packet.wind_direction);
        client.print("/bar/");      client.print(packet.pressure);
        client.print("/rain/");     client.print(packet.rainfall);
        client.print("/rainrate/"); client.print(packet.rain_rate);
        client.print("/uvi/");      client.print(packet.uv_index);
        client.print("/solarrad/"); client.print(packet.solar_radiation);

        // Close and send the GET request
        client.println("/ HTTP/1.1");
        client.println("Host: api.weathercloud.net");
        client.println("Connection: close");
        client.println();
        
        Serial.println("Data pushed to Weathercloud successfully.\n");
    }

    // Timeinfo instance
    static struct tm timeinfo;

private:
    // Enables the RF chip
    static void enable_rf()
    {
        WiFi.mode(WIFI_OFF);
    }

    // Disables the RF chip
    static void disable_rf()
    {
        WiFi.mode(WIFI_STA);
    }

    // WifiClient instance
    static WiFiClient client;
};

WiFiClient Network::client;
tm Network::timeinfo;