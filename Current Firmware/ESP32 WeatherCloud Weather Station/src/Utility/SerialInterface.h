/*!
*    @file SerialInterface.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to print debug messages and variables to the Serial console.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    2) Packet.h header file (a part of the ESP32 Weathercloud Weather Station project)
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once


// Contains commands for printing debug messages to the console
class SerialInterface {
public:
    // Short version of the Serial.print() method
    template <typename T>
    static void print(const T& t)
    {
        Serial.print(t);
    }

    // Short version of the Serial.println() method
    template <typename T>
    static void println(const T& t)
    {
        Serial.println(t);
    }
    
    // Initialize the serial interface at the default baud rate
    static void begin()
    {
        Serial.begin(115200);

        empty_line(2);
        println("System Enabled.");
        empty_line(1);
    }

    // Prints a given number of empty lines
    static void empty_line(int lines)
    {
        for(int i = 0; i < lines; i++)
        {
            println("");
        }
    }

    // Prints the whole data packet
    static void print_packet(Packet packet)
    {
        print("Air temperature [°C]: ");        println(packet.temperature);
        print("Ground temperature [°C]: ");     println(packet.temperature_inside);
        print("Wind chill [°C]: ");             println(packet.wind_direction);
        print("Dew point [°C]: ");              println(packet.dew_point);
        print("Heat index [°C]: ");             println(packet.heat_index);
        print("Humidity [%]: ");                println(packet.humidity);
        print("Wind speed [m/s]: ");            println(packet.wind_speed);
        print("Wind direction [°]: ");          println(packet.wind_direction);
        print("Barometric pressure [hPa]: ");   println(packet.pressure);
        print("Rainfall (per day) [mm/m2]: ");  println(packet.rainfall);
        print("Rainrate (per hour) [mm/m2]: "); println(packet.rain_rate);
        print("UV index [Index]: ");            println(packet.uv_index);
        print("Solar radiation [W/m2]: ");      println(packet.solar_radiation);
        println("");
    }

    // Program end
    template <typename T>
    static void program_end(const T& message)
    {
        println(message);
        println("System entering hibernation mode. Please resolve the issue and restart the system.");
        esp_deep_sleep_start();
    }
};