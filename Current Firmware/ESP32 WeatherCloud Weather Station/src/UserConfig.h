/*!
*    @file UserConfig.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to configure different parameters in this program, such as the
*    network authorization keys and Weathercloud API keys.
*
*    @section Dependencies
*
*    This file has no dependencies.
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

// Wi-fi network authorization
const char* ssid     = "Kubkov Huawei";                 // Type in your Wi-Fi network SSID (name)
const char* password = "internet";             // Type in your Wi-Fi network password

// Weathercloud API authorization
const char* Weathercloud_ID  = "your-weathercloud-key";  //Copy and paste your Weathercloud ID here
const char* Weathercloud_KEY = "your-weathercloud-key";  //Copy and paste your Weathercloud KEY here

// Timezone configuration
const long  gmtOffset_sec = 3600;                       //Put here your local time offset in seconds (example GMT+1 = +1 * 3600s = 3600s)
const int   daylightOffset_sec = 3600;                  //Do you have daylight saving time? Yes = 3600, No = 0