/*!
*    @file Temperature.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to interface the 2 DS18B20 sensors inside of the station.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project) 
*    2) SerialInterface.h header file (a part of the ESP32 Weathercloud Weather Station project)  
*    3) OneWire temperature interface library. Located at: <https://github.com/PaulStoffregen/OneWire>
*    4) DallasTemperature DS18B20 library. Located at: <https://github.com/milesburton/Arduino-Temperature-Control-Library/blob/master/DallasTemperature.h>
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// Temperature sensor libraries
#include <OneWire.h>
#include <DallasTemperature.h>


// Used to interface the 2 DS18B20 sensors inside of the station
class Temperature {
public:
    // Initializes the DS18B20 temperature sensors
    static void begin()
    {
        temp_sensors.begin();
        if(!temp_sensors.getAddress(air, 1))    SerialInterface::program_end("DS18B20 air temperature sensor initialization failed!");
        if(!temp_sensors.getAddress(ground, 0)) SerialInterface::program_end("DS18B20 ground temperature sensor initialization failed!");
        
        // Sets resolution of the temperature sensors
        temp_sensors.setResolution(air, 15);
        temp_sensors.setResolution(ground, 15);
    }

    // Measures air temperature
    static float measure_air()
    {
        return temp_sensors.getTempC(air);
    }

    // Measures ground temperature
    static float measure_ground()
    {
        return temp_sensors.getTempC(ground);
    }

    // Calculates and returns wind chill
    static float calculate_wind_chill(float wind_speed)
    {
        float temp = measure_air();

        // Convert wind speed to km/h
        float wind_speed_kph = wind_speed * 3.6;
        
        // If wind speed or temperature reach a certain threshold, return air temperature
        if(wind_speed < 0.5) return temp;
        if(temp > 30) return temp;

        // Calculate and return wind chill
        return (13.12 + (0.62 * temp) - (11.37 * (pow(wind_speed_kph, 0.16))) + (0.39 * temp * (pow(wind_speed_kph, 0.16))));;
    }

    // Calculates and returns dew point
    static float calculate_dew_point(float hum)
    {
        float temp = measure_air();
        double gamma = log(hum / 100) + ((17.62 * temp) / (243.5 + temp));
        return (243.5 * gamma / (17.62 - gamma));
    }

    // Calculates and returns heat index
    static float calculate_heat_index(float hum)
    {
        // Constants for calculating the heat index
        const double c[9] = { -8.78469475556, 1.61139411, 2.33854883889, -0.14611605, -0.012308094, -0.0164248277778, 0.002211732, 0.00072546, -0.000003582 };

        float temp = measure_air();
        return c[0] + (c[1] * temp) + (c[2] * hum) + (c[3] * temp * hum) + (c[4] * sq(temp)) + (c[5] * sq(hum)) + (c[6] * sq(temp) * hum) + (c[7] * temp * sq(hum)) + (c[8] * sq(temp) * sq(hum));
    }

private:
    // Object instances
    static OneWire oneWire;
    static DallasTemperature temp_sensors;
    static DeviceAddress ground;
    static DeviceAddress air;
};

// Temperature bus pin
const int temperature_pin = 4;

// Temp sensor config
OneWire Temperature::oneWire = OneWire(temperature_pin);
DallasTemperature Temperature::temp_sensors = DallasTemperature(&oneWire);

// Different sensor addresses, you might need to edit these to your needs
DeviceAddress Temperature::ground = { 0x28,  0xD4,  0xB0,  0x26,  0x0,  0x0,  0x80,  0xBC };
DeviceAddress Temperature::air = { 0x28,  0xF4,  0xBC,  0x26,  0x0,  0x0,  0x80,  0x2B };