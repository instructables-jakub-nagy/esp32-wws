/*!
*    @file Solar.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to interface the UV and solar radiation sensors inside the weather station.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    2) BHT1750 solar radiation sensor library. Located at <https://github.com/claws/BH1750>.
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// Sensor libraries
#include <BH1750.h>


// Used to interface the UV and solar radiation sensors inside the weather station
class Solar {
public:
    // Init the solar radiation sensor
    static void begin()
    {
        if(!solar_meter.begin()) SerialInterface::program_end("BHT1750 solar radiation sensor initialization failed!");
    }

    // Measures solar radiation trough I2C
    static float measure_solar_radiation()
    {
        float lux = solar_meter.readLightLevel();
        
        // Return calculated solar radiation in W/m2
        return lux * 0.79;
    }

    // Measures raw ADC value and converts it to UV index
    static float measure_uv_index()
    {
        float raw_uv_reading = (analogRead(UV_INPUT) * (3.3 / 4095));

        // Return the min/max UV index if ADC reading reaches a certain threshold
        if(raw_uv_reading > 2.46) return 11;
        else if(raw_uv_reading < 1) return 0;

        // Return calculated UV index
        return (raw_uv_reading - 1) * 7.5;
    }

private:
    // BH1750 instance
    static BH1750 solar_meter;

    // UV sensor input pin
    static const int UV_INPUT = 34;
};

BH1750 Solar::solar_meter;