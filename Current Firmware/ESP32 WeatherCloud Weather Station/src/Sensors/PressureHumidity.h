/*!
*    @file PressureHumidity.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to interface the BME280 sensor, which measures temperature, humidity
*    and pressure. Only measured humidity and pressure is used, hence the name of this file.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    2) Wire I2C communication library. Is a part of the Arduino framework.
*    3) Seed BME280 sensor library. Located at: <https://github.com/Seeed-Studio/Grove_BME280>
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// Sensor libraries
#include <Wire.h>
#include <Seeed_BME280.h>


// Used to interface the BME280 sensor
class PressureHumidity {
public:
    // Initialize the humidity and pressure sensor BME280
    static void begin()
    {
        if(!Wire.begin())  SerialInterface::program_end("I2C interface initialization failed!");
        if(!bme280.init()) SerialInterface::program_end("BME280 environment sensor initialization failed!");
    }

    // Measures relative humidity
    static float measure_humidity()
    {
        return bme280.getHumidity();
    }

    // Measures pressure in hPa
    static float measure_pressure()
    {
        return bme280.getPressure() / 100.0f;
    }

private:
    // BME280 instance
    static BME280 bme280;
};

BME280 PressureHumidity::bme280;