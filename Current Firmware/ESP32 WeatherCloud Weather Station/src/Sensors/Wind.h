/*!
*    @file Wind.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to measure the analog values from the wind sensors and interpret
*    them as wind speed and wind direction.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project) 
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once


// Used to measure the analog values from the wind sensors and interpret them as wind speed and wind direction
class Wind {
public:

    static float measure_wind_speed()
    {
        unsigned long lastMillis = 0;
        float wind_speed;

        SerialInterface::println("Starting wind speed measurement period.");

        attachInterrupt(WIND_SPEED_INPUT, wind_click_interrupt, FALLING);
        lastMillis = xTaskGetTickCount();
        while(xTaskGetTickCount() - lastMillis < 10000) {}
        detachInterrupt(WIND_SPEED_INPUT);

        SerialInterface::println("Wind speed measurement complete.\n");

        return clicks * 0.666;
    }

    // Rain gauge interrupt handler, triggers when the rain gauge clicks
    static void wind_click_interrupt()
    {
        clicks++;
    }

    static int measure_wind_direction()
    {
        int raw_wind_direction_reading = analogRead(WIND_DIRECTION_INPUT);
        int wind_direction = 0;

        if(raw_wind_direction_reading > 2155 && raw_wind_direction_reading < 2207) wind_direction = 0;
        else if (raw_wind_direction_reading > 502  && raw_wind_direction_reading < 554) wind_direction = 22.5;
        else if (raw_wind_direction_reading > 649  && raw_wind_direction_reading < 699) wind_direction = 45;
        else if (raw_wind_direction_reading > 49 && raw_wind_direction_reading < 90) wind_direction = 112.5;
        else if (raw_wind_direction_reading > 89 && raw_wind_direction_reading < 111) wind_direction = 135;
        else if (raw_wind_direction_reading > 0 && raw_wind_direction_reading < 11) wind_direction = 157.5;
        else if (raw_wind_direction_reading > 242 && raw_wind_direction_reading < 280) wind_direction = 180;
        else if (raw_wind_direction_reading > 159 && raw_wind_direction_reading < 211) wind_direction = 202.5;
        else if (raw_wind_direction_reading > 1239 && raw_wind_direction_reading < 1301) wind_direction = 225;
        else if (raw_wind_direction_reading > 1129 && raw_wind_direction_reading < 1161) wind_direction = 247.5;
        else if (raw_wind_direction_reading > 4069 && raw_wind_direction_reading < 5001) wind_direction = 270;
        else if (raw_wind_direction_reading > 2470 && raw_wind_direction_reading < 2516) wind_direction = 292.5;
        else if (raw_wind_direction_reading > 3101 && raw_wind_direction_reading < 3321) wind_direction = 315;
        else if (raw_wind_direction_reading > 1629 && raw_wind_direction_reading < 1656) wind_direction = 337.5;
        else if(raw_wind_direction_reading == 0) wind_direction = 90;

        return wind_direction;
    }

private: 
    // Wind sensor input pins
    static const int WIND_SPEED_INPUT = 26;
    static const int WIND_DIRECTION_INPUT = 35;

    // Number of measure anemometer clicks
    static int clicks;
};